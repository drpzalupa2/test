<?php

class My
{

    public function pay()
    {
        $result = $this->execute('https://www.alfaportal.ru/card2card/ptpl/repka/initial.html');
        if (preg_match('<input type="hidden"  name="ssn_token" value="(.*?)"  />', $result, $match)) {
            $data = array(
                'partner_name' => 'ruru',
                'ssn_token' => $match[1],
                'form' => 'form',
                'tfr_card_src_num' => '4890 4944 2158 5175',
                'tfr_card_src_exp_month' => '02',
                'tfr_card_src_exp_year' => '2019',
                'tfr_card_dst_num' => '4276 8800 4674 8699',
                'tfr_card_dst_exp_month' => '',
                'tfr_card_dst_exp_year' => '',
                'tfr_amount' => 50000,
                'service_terms_ok' => 'on',
                'action_proceed' => 'Отправить деньги',
                'javax.faces.ViewState' => 'j_id1'
            );

            $result = $this->execute('https://www.alfaportal.ru/card2card/ptpl/repka/initial.html', $data);


            print_r($result);
        }
    }

    public function execute($url, $data = array())
    {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36 OPR/43.0.2442.991');
//        curl_setopt($curl, CURLOPT_COOKIEFILE, 'cookie.txt');
//        curl_setopt($curl, CURLOPT_COOKIEJAR, 'cookie.txt');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);

        if ($data) {
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        }

        $result = curl_exec($curl);
        $error = curl_error($curl);
        $errorNo = curl_errno($curl);
        curl_close($curl);

        print_r($error);
//        print_r($errorNo);

        return $result;
    }
}

$app = new My;
$app->pay();